# exemples d'imports
import random as rand
from random import randint as randi

"""
Organisation des classes : 

[Armes]    [Magies]
   |         |
[Guerrier] [Mage]
     \       /
  [Pyromancien]
  
  [Personnage] <- Guerrier || Mage || Pyromancien
"""

class Magies:
    
    disponibles = set(["feu","terre","air","eau"])
    
    def __init__(self, maitrises):
        if(self._maitrisesNotInMagies(maitrises)):
            raise Exception('maitrise(s) inconnue(s) : ' + ", ".join(maitrises))
        self.maitrises = maitrises
        
    def _maitrisesNotInMagies(self, maitrises):
        return not maitrises.intersection(Magies.disponibles) == maitrises
    
    def sortilege(self):
        magie = rand.choice(list(self.maitrises))
        if(magie == 'feu') : return "envoie une boule de feu sur"
        elif(magie == 'terre') : return "provoque un séisme en dessous de"
        elif(magie == 'air') : return "fait tomber la foudre sur"
        elif(magie == 'eau') : return "projete un torrent d'eau sur"
   
class Armes:
    
    disponibles = set(["épée","baguette","baton"])
    
    def __init__(self, arme):
        if(self._armeNotInArmes(arme)):
            raise Exception('arme non disponible : ' + arme)
        self.arme = arme
        
    def _armeNotInArmes(self, arme):
        return not arme in Armes.disponibles
    
    def action(self):
        if(self.arme == 'épée') : return "frappe avec son épée"
        elif(self.arme == 'baguette') : return "mitraille avec sa baguette"
        elif(self.arme == 'baton') : return "frappe avec son baton"
    
class Mage(Magies):
    def __init__(self, magies = set(["feu","terre","air","eau"]) ):
        Magies.__init__(self, magies)
        self.facteurInitiative = 2
    
    def attaque(self):
        return super().sortilege()
        
class Guerrier(Armes):
    def __init__(self, arme = 'épée'):
        super().__init__(arme)
        self.facteurInitiative = 1
        
    def attaque(self):
        return super().action()
      
class Pyromancien(Guerrier, Mage):
    def __init__(self, arme = 'baton', magie = set(['feu'])):
        # super().__init__(self, ...) est ambigue ici
        Guerrier.__init__(self, arme)
        Mage.__init__(self, magie)
        self.facteurInitiative = 2
        
    # surcharge de attaque() 
    def attaque(self):
        #return f"{super().attaque()}" # retourne l'attaque du guerrier, car
        #en héritage multiple avec un super() dans Python Class(A,B,..,N) 
        #c'est A qui est appelé par défaut avec super()
        return f"{super().action()} puis {super().sortilege()}"
        
class Personnage:    
    def __init__(self, name, classe):
        self.name = name
        self.classe = classe
        self.initiative = randi(1,10)
        self.isDead = False
        
    def attaque(self, cible):
        cible.isDead = f"{cible.name} meurt dans d'atroces souffrances !"
        return f"{self.name} {self.classe.attaque()} {cible.name}"    
        
    # __lt__(self, y) : self < y 
    # __le__(self, y) : self <= y 
    # __eq__(self, y) : self == y 
    # __nq__(self, y) : self != y 
    # __gt__(self, y) : self > y 
    # __ge__(self, y) : self >= y 
    def __gt__(self, personnage):
        x = self.initiative * self.classe.facteurInitiative
        y = personnage.initiative * personnage.classe.facteurInitiative
        return x > y

    def __str__(self):
        return f"""
    Je suis le {type(self.classe).__name__} {self.name} !
    Mon initiative est de {round(self.initiative * self.classe.facteurInitiative,1)}
            """
    
#print(Magies) avec __str__ ne fonctionne pas car Magies n'est pas instanciée
print("Les magies dispos sont : " + ", ".join(Magies.disponibles))
print("Les armes dispos sont : " + ", ".join(Armes.disponibles))

Personnages = [
    Personnage('Elminster', Mage()),
    Personnage('Bob', Pyromancien())
]

# > fonctionnerai aussi ici, definir __gt__ gere < et gere du coup 
# automatiquement > et >= (en cas d'égalité, Personnages[0] a l'initiative)
if(Personnages[1] > Personnages[0]): 
    Personnages[0],Personnages[1] = Personnages[1],Personnages[0]

for personnage in Personnages:
    print(personnage)

print(f"{Personnages[0].attaque(Personnages[1])}")
print(Personnages[1].isDead)